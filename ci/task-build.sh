#!/bin/bash

set -e -x -u

yarn --no-lockfile --registry http://verdaccio-verdaccio:4873
yarn build