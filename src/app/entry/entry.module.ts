import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ClrVerticalNavModule } from '@clr/angular';
import { EntryRoutingModule } from './entry-routing.module';
import { EntryComponent } from './entry.component';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';

/**
 * EntryModule is the entry point for this bundled dashboard.
 * It is important to name your enty point the EntryModule
 * because the @hack/core's module loader will references it
 * by EntryModule.
 */
@NgModule({
  imports: [CommonModule, EntryRoutingModule, ClrVerticalNavModule],
  declarations: [EntryComponent, OneComponent, TwoComponent],
  providers: [{ provide: 'Entry', useValue: EntryComponent }]
})
export class EntryModule {}
